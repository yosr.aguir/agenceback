<?php

namespace App\Controller;

use App\Form\LocalCommercialType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Annonce;
use App\Entity\Images;
use App\Repository\ImagesRepository;


class LocalCommercialController extends AbstractController
{
    /**
     * @Route("/local/commercial", name="local_commercial")
     */
    public function index()
    {
        return $this->render('local_commercial/index.html.twig', [
            'controller_name' => 'LocalCommercialController',
        ]);
    }

    /**
     * @Route("/localCommercial/new" , name="new_local_commercial")
     * Method({"GET" , "POST"})
     */

    public function new( Request $request)
    {
        $annonce = new Annonce() ;
        $form = $this->createForm(LocalCommercialType::class, $annonce);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $annonce = $form->getData() ;

            $entityManager = $this->getDoctrine()->getManager();
            $annonce->setIdAdmin($this->getUser());
            $entityManager->persist($annonce);
            $entityManager->flush();


            return $this->redirectToRoute('annonce_index', array('id' => $annonce->getId()));
        }


        return $this ->render('annonce_new',['id'=>6]);
    }
    /**
     * @Route("/LocalCommercial/edit/{id}" , name="local_commercial_edit")
     * Method({"GET" , "POST"})
     */
    public function edit(Request $request, Annonce $annonce, ImagesRepository $imagesRepository)
    {

        $images = $imagesRepository->findBy(['idAnnonce'=>$annonce]);
        $form = $this->createForm(LocalCommercialType::class, $annonce);
        $form->handleRequest($request);

        if ($form->isSubmitted() ) {
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                if ($_FILES['picProfilLocal']['name'] !== '') {
                    $path = $_FILES['picProfilLocal']['name'];
                    $type = pathinfo($path, PATHINFO_EXTENSION);
                    $data = file_get_contents($_FILES['picProfilLocal']['tmp_name']);
                    $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
                    $annonce->setImgProfil($base64);

                }
                $em->persist($annonce);
                $em->flush();

                for ($i = 0; $i < sizeof($_FILES['picCommercial']['name']); $i++) {
                    $path = $_FILES['picCommercial']['name'][$i];
                    $type = pathinfo($path, PATHINFO_EXTENSION);
                    $data = file_get_contents($_FILES['picCommercial']['tmp_name'][$i]);
                    $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
                    $image = new Images();
                    $image->setImg($base64)
                        ->setIdAnnonce($annonce);

                    $em->persist($image);
                    $em->flush();
                    $this->addFlash(
                        'info',
                        'Annonce Bien Modifiée'
                    );
                }
                return $this->redirectToRoute('annonce_index', array('id' => $annonce->getId()));
            } else {
                return $this->render('local_commercial/edit.html.twig', array(
                    'annonce' => $annonce,
                    'form' => $form->createView(),
                    'images'=> $images
                ));
            }
        }
        return $this->render('local_commercial/edit.html.twig', array(
            'annonce' => $annonce,
            'form' => $form->createView(),
            'images'=> $images
        ));
    }

    /**
     * @Route("/{id}", name="local_commercial_show", methods={"GET"},requirements={"id":"\d+"})
     */
    public function show(Annonce $annonce , ImagesRepository $imagesRepository): Response
    {

        $images = $imagesRepository->findBy(['idAnnonce'=>$annonce]);
        return $this->render('local_commercial/show.html.twig', [
            'annonce' => $annonce,
            'id' => 6,
            'images'=> $images,
        ]);
    }
}

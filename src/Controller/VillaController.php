<?php

namespace App\Controller;

use App\Form\VillaType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Annonce;
use App\Entity\Avis;
use App\Entity\Images;
use App\Repository\ImagesRepository;


class VillaController extends AbstractController
{
    /**
     * @Route("/villa", name="villa")
     */
    public function index()
    {
        return $this->render('villa/index.html.twig', [
            'controller_name' => 'VillaController',
        ]);
    }


    /**
     * @Route("/villa/new" , name="new_villa",requirements={"id":"\d+"})
     * Method({"GET" , "POST"})
     */

    public function new( Request $request)
    {
        $annonce = new Annonce() ;
        $form = $this->createForm(VillaType::class, $annonce);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $annonce = $form->getData() ;

            $entityManager = $this->getDoctrine()->getManager();
            $annonce->setIdAdmin($this->getUser());
            $entityManager->persist($annonce);
            $entityManager->flush();


            return $this->redirectToRoute('annonce_index');
        }

        return $this ->render('annonce_new',['id'=>2]);
    }
    /**
 * @Route("/villa/edit/{id}" , name="villa_edit",requirements={"id":"\d+"})
 * Method({"GET" , "POST"})
 */
    public function edit(Request $request, Annonce $annonce , ImagesRepository $imagesRepository)
    {

        $images = $imagesRepository->findby(['idAnnonce' =>$annonce]);
        $form = $this->createForm(VillaType::class, $annonce);
        $form->handleRequest($request);

        if ($form->isSubmitted() ){
            if($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                if ($_FILES['picProfilVilla']['name'] !== '') {
                    $path = $_FILES['picProfilVilla']['name'];
                    $type = pathinfo($path, PATHINFO_EXTENSION);
                    $data = file_get_contents($_FILES['picProfilVilla']['tmp_name']);
                    $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
                    $annonce->setImgProfil($base64);

                }
            $em->persist($annonce);
            $em->flush();

            for ($i=0; $i<sizeof($_FILES['picVilla']['name']); $i++){
                $path = $_FILES['picIndus']['name'][$i];
                $type = pathinfo($path, PATHINFO_EXTENSION);
                $data = file_get_contents($_FILES['picIndus']['tmp_name'][$i]);
                $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
                $image = new Images();
                $image->setImg($base64)
                    ->setIdAnnonce($annonce);


                $em->persist($image);
                $em->flush();

                $this->addFlash(
                    'info',
                    'Annonce Bien Modifiée'
                );
            }

            return $this->redirectToRoute('annonce_index', array('id' => $annonce->getId()));
        }else{
                return $this->render('villa/edit.html.twig', array(
                    'annonce' => $annonce,
                    'form' => $form->createView(),
                    'images'=>$images,
                ));
            }
        }
        return $this->render('villa/edit.html.twig', array(
            'annonce' => $annonce,
            'form' => $form->createView(),
            'images'=>$images,
        ));
    }

    /**
     * @Route("/{id}", name="villa_show", methods={"GET"},requirements={"id":"\d+"})
     */
    public function show(Annonce $annonce , ImagesRepository $imagesRepository): Response
    {
        $images = $imagesRepository->findby(['idAnnonce' =>$annonce]);
        return $this->render('villa/show.html.twig', [
            'annonce' => $annonce,
            'id' => 2,
            'images'=>$images ,
        ]);
    }
}

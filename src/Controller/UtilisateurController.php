<?php

namespace App\Controller;

use App\Entity\Utilisateur;
use App\Form\UtilisateurAddType;
use App\Form\UtilisateurType;
use App\Repository\UtilisateurRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @Route("/utilisateur")
 */
class UtilisateurController extends AbstractController
{
    /**
     * @Route("/", name="utilisateur_index", methods={"GET"})
     */
    public function index(UtilisateurRepository $utilisateurRepository): Response
    {


        return $this->render('utilisateur/index.html.twig', [
            'utilisateurs' => $utilisateurRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="utilisateur_new", methods={"GET","POST"})
     */
     public function new(Request $request, UserPasswordEncoderInterface $encoder, \Swift_Mailer $mailer): Response
    {
        $utilisateur = new Utilisateur();
        $form = $this->createForm(UtilisateurAddType::class, $utilisateur);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // Convert picture to base64

            $path = $_FILES['utilisateur_add']['name']['img'];
            $type = pathinfo($path, PATHINFO_EXTENSION);
            $data = file_get_contents($_FILES['utilisateur_add']['tmp_name']['img']);
            $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);

            $pwd = self::getPassword();
            $hash = $encoder->encodePassword($utilisateur,$pwd);
            $utilisateur->setPassword($hash)
                        ->setImg($base64);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($utilisateur);
            $entityManager->flush();

            $message = (new \Swift_Message('Hello Email'))
                ->setFrom('gsi.mail2019@gmail.com')
                ->setTo($utilisateur->getEmail())
                ->setBody(
                    $this->renderView(
                    // templates/emails/registration.html.twig
                        'emails/registration.html.twig',
                        [
                            'email' => $utilisateur->getEmail(),
                            'pwd' => $pwd,
                        ]
                    ),
                    'text/html'
                );

            $mailer->send($message);

            return $this->redirectToRoute('utilisateur_index');
        }

        return $this->render('utilisateur/new.html.twig', [
            'utilisateur' => $utilisateur,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="utilisateur_show", methods={"GET"})
     */
    public function show(Utilisateur $utilisateur): Response
    {
        return $this->render('utilisateur/show.html.twig', [
            'utilisateur' => $utilisateur,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="utilisateur_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, UserPasswordEncoderInterface $encoder, \Swift_Mailer $mailer, Utilisateur $utilisateur): Response
    {
        $form = $this->createForm(UtilisateurType::class, $utilisateur);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $entityManager = $this->getDoctrine()->getManager();
            if ($_FILES['pic']['name'] !== '') {
                $path = $_FILES['pic']['name'];
                $type = pathinfo($path, PATHINFO_EXTENSION);
                $data = file_get_contents($_FILES['pic']['tmp_name']);
                $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
                $utilisateur->setImg($base64);
            }
            $entityManager->persist($utilisateur);
            $entityManager->flush();

            return $this->redirectToRoute('utilisateur_index');
        }

        return $this->render('utilisateur/edit.html.twig', [
            'utilisateur' => $utilisateur,
            'form' => $form->createView(),
        ]);
    }


    /**
     * @Route("/{id}", name="utilisateur_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Utilisateur $utilisateur): Response
    {
        if ($this->isCsrfTokenValid('delete'.$utilisateur->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($utilisateur);
            $entityManager->flush();
        }

        return $this->redirectToRoute('utilisateur_index');
    }


    /**
     * @Route("/{id}/status" , name="utilisateur_status")

     */
    public function activerAction(Utilisateur $utilisateur)
    {
        $utilisateur->setActive(!$utilisateur->getActive());
        $em = $this->getDoctrine()->getManager();
        $em->persist($utilisateur);
        $em->flush();

        return $this->redirectToRoute('utilisateur_index');
    }


    public static function getPassword($dPassLength = 8)
    {
        $sPasswd     = "";
        $sCharacters = "abcdefghjkmnopqrstuvwxyz0123456789";

        for ($i = 1; $i <= $dPassLength; $i++) {
            $randPosition = mt_rand(0, (strlen($sCharacters) - 1));
            $sPasswd      .= $sCharacters[$randPosition];
        }

        return $sPasswd;
    }




}

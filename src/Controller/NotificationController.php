<?php

namespace App\Controller;

use App\Repository\NotificationRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class NotificationController extends AbstractController
{
    /**
     * @Route("/notification", name="notification")
     */

    public function index()
    {
        return $this->render('notification/index1.html.twig', [
            'controller_name' => 'NotificationController',
        ]);
    }
    public function commentaire(NotificationRepository $repository)
    {
        $all = $repository->findBy(['vu'=>0]);
        $notification = [];
        foreach ( $all as $n){
            $ch = $n->getNotification();
            $msg = substr($ch, 0, strpos($ch, ':') - 1);

            if ($msg == "Vous avez un nouveau commentaire"){
                array_push($notification,["msg"=>$msg,"annonce"=>substr($ch, strpos($ch, ':') + 1, strlen($msg))]);
            }
        }
        return $this->render('notification/index.html.twig', [
            'notifications'=>$notification,
            'nbr'=>count($notification)
        ]);
    }

    public function message(NotificationRepository $repository)
    {
        $all = $repository->findBy(['vu'=>0]);
        $notification = [];
        foreach ( $all as $n){
            $ch = $n->getNotification();
            $msg = substr($ch, 0, strpos($ch, ':') - 1);

            if ($msg == "Vous avez recu un nouveau message"){
                if (substr($ch, strpos($ch, ':') + 1, strlen($msg))=='User'){
                    array_push($notification,["msg"=>$msg]);
                }
            }
        }
        return $this->render('notification/index2.html.twig', [
            'messages'=>$notification,
            'nbr'=>count($notification)
        ]);
    }

    /**
     * @Route("/vu", name="message_vu")
     */
    public function voir(NotificationRepository $repository){

        //dump("her");exit;
        $em = $this->getDoctrine()->getManager();

        $notifications = $repository->findBy(['vu'=>0]);
        foreach ( $notifications as $n){
            $ch = $n->getNotification();
            $msg = substr($ch, 0, strpos($ch, ':') - 1);

            if ($msg == "Vous avez recu un nouveau message"){
                if (substr($ch, strpos($ch, ':') + 1, strlen($msg))=='User'){
                    $n->setVu(1);
                    $em->persist($n);
                    $em->flush();
                }
            }
        }

        return $this->redirectToRoute('message',['id'=> 0]);
    }


}

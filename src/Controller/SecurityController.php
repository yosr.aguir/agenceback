<?php

namespace App\Controller;

use App\Entity\Admin;
use App\Repository\AnnonceRepository;
use App\Repository\MessageRepository;
use App\Repository\UtilisateurRepository;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use phpDocumentor\Reflection\Types\This;
use http\Env\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class SecurityController extends AbstractController
{
    /**
     * @Route("/connexion", name="security_login")
     */
    public function login(AuthenticationUtils $authenticationUtils,RequestStack $requestStack)
    {
        $session = $requestStack->getCurrentRequest()->getSession();
        if ($session->get('_security_main')) {
            return $this->redirectToRoute('dashboard');
        } /*else {
            dump($session);exit;
        }*/
        return $this->render('security/login.html.twig', [
            'error'=>$authenticationUtils->getLastAuthenticationError(),
            'controller_name' => 'SecurityController',
        ]);
    }

    /**
     * @Route("/deconnexion", name="security_logout")
     */
    public function logout()
    {

    }

    /**
     * @Route("/", name="dashboard")
     */
    public function index(UtilisateurRepository $repository , AnnonceRepository $annonceRepository , MessageRepository $messageRepository)
    {
        $utilisateurs = $repository->findAll();
        $annonces = $annonceRepository ->findAll();
        $messages = $messageRepository ->findAll();
        $nbrUsers= count($utilisateurs);
        $nbrAnnonces = count($annonces);
        $nbrmessages = count($messages);
        return $this->render('dashboard.html.twig',[
            'nbrUser'=>$nbrUsers,
            'nbrAnnonces'=>$nbrAnnonces,
            'nbrmessages'=>$nbrmessages,
        ]);
    }

    /**
     * @Route("/register", name="security_register")
     */
    public function inscri(UserPasswordEncoderInterface $encoder)
    {
        /* $admin = new Admin();
         $hash = $encoder->encodePassword($admin,'admin');

         $admin->setFirstname("yosr")
                 ->setLastname("aguir")
                 ->setEmail("aguiryosr@gmail.com")
             ->setPassword($hash);
         $em = $this->getDoctrine()->getManager();
         $em->persist($admin);
         $em->flush();
         return $this->render('security/login.html.twig', [
             'controller_name' => 'SecurityController',
         ]);*/
    }


}

<?php

namespace App\Controller;

use App\Form\TerrainAgricoleType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Annonce;
use App\Entity\Images;
use App\Repository\ImagesRepository;


class TerrainAgricoleController extends AbstractController
{
    /**
     * @Route("/terrain/agricole", name="terrain_agricole")
     */
    public function index()
    {
        return $this->render('terrain_agricole/index.html.twig', [
            'controller_name' => 'TerrainAgricoleController',
        ]);
    }

    /**
     * @Route("/terrainAgricole/new" , name="new_terrain_agricole")
     * Method({"GET" , "POST"})
     */
    public function new(Request $request)
    {
        $annonce = new Annonce();
        $form = $this->createForm(TerrainAgricoleType::class, $annonce);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $annonce = $form->getData();

            $entityManager = $this->getDoctrine()->getManager();
            $annonce->setIdAdmin($this->getUser());
            $entityManager->persist($annonce);
            $entityManager->flush();


            return $this->redirectToRoute('annonce_index');
        }
        return $this->render('annonce_new', ['id' => 3]);
    }

    /**
     * @Route("/terrainAgricole/edit/{id}" , name="edit_terrain_agricole",requirements={"id":"\d+"})
     * Method({"GET" , "POST"})
     */
    public function edit(Request $request, Annonce $annonce, ImagesRepository $imagesRepository)
    {

        $images = $imagesRepository->findBy(['idAnnonce' => $annonce]);
        $form = $this->createForm(TerrainAgricoleType::class, $annonce);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();

                if ($_FILES['picProfilAgricole']['name'] !== '') {
                    $path = $_FILES['picProfilAgricole']['name'];
                    $type = pathinfo($path, PATHINFO_EXTENSION);
                    $data = file_get_contents($_FILES['picProfilAgricole']['tmp_name']);
                    $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
                    $annonce->setImgProfil($base64);
                }
                $em->persist($annonce);
                $em->flush();

                for ($i = 0; $i < sizeof($_FILES['picAgrico']['name']); $i++) {
                    $path = $_FILES['picAgrico']['name'][$i];
                    $type = pathinfo($path, PATHINFO_EXTENSION);
                    $data = file_get_contents($_FILES['picAgrico']['tmp_name'][$i]);
                    $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);

                    $image = new Images();
                    $image->setImg($base64)
                        ->setIdAnnonce($annonce);
                    $em->persist($image);
                    $em->flush();

                    $this->addFlash(
                        'info',
                        'Annonce Bien Modifiée'
                    );
                }
                return $this->redirectToRoute('annonce_index', array('id' => $annonce->getId()));
            } else {
                return $this->render('terrain_agricole/edit.html.twig', array(
                    'annonce' => $annonce,
                    'form' => $form->createView(),
                    'images' => $images
                ));
            }


        }
        return $this->render('terrain_agricole/edit.html.twig', array(
            'annonce' => $annonce,
            'form' => $form->createView(),
            'images' => $images
        ));
    }

    /**
     * @Route("/{id}", name="show_terrain_agricole", methods={"GET"},requirements={"id":"\d+"})
     */
    public
    function show(Annonce $annonce, ImagesRepository $imagesRepository): Response
    {
        $images = $imagesRepository->findBy(['idAnnonce' => $annonce]);
        return $this->render('terrain_agricole/show.html.twig', [
            'annonce' => $annonce,
            'id' => 4,
            'images' => $images,
        ]);
    }
}

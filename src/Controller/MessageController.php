<?php

namespace App\Controller;

use App\Entity\Message;
use App\Repository\MessageRepository;
use App\Repository\UtilisateurRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class MessageController extends AbstractController
{
    /**
     * @Route("/message/{id}", name="message")
     */
    public function index($id, MessageRepository $repository, UtilisateurRepository $utilisateurRepository)
    {
        if ($id == 0){
            $users= $repository->getMessage();
            $user = $utilisateurRepository->find($users[0]['user']);
            $msgFirstUser = $repository->findBy(['user'=>$users[0]['user']]);
            //dump($msg);exit;
            return $this->render('message/index.html.twig', [
                'users' => $users,
                'user' => $user,
                'msgFirstUser' => $msgFirstUser
            ]);
        } else {
            $users= $repository->getMessage();
            $user = $utilisateurRepository->find($id);
            $msgFirstUser = $repository->findBy(['user'=>$id]);
            //dump($msg);exit;
            return $this->render('message/index.html.twig', [
                'users' => $users,
                'user' => $user,
                'msgFirstUser' => $msgFirstUser
            ]);
        }

    }
    /**
     * @Route("/message/envoyer/{id}", name="message_envoyer")
     */
    public function envoyerMsg($id, MessageRepository $repository, UtilisateurRepository $utilisateurRepository, Request $request)
    {
        $msg = $request->request->get('msg');
        $user = $utilisateurRepository->find($id);
        $message = new Message();
        $message->setRecuPar('User')
                ->setEnvoyerPar('Admin')
                ->setDataTime(new \DateTime())
                ->setUser($user)
                ->setMsg($msg);

        $this->getDoctrine()->getManager()->persist($message);
        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute('message',['id'=>$id]);
    }



}

<?php

namespace App\Controller;
use App\Entity\Avis;
use App\Entity\Annonce;
use App\Repository\AvisRepository;
use App\Repository\AnnonceRepository;
use App\Repository\NotificationRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AvisController extends AbstractController

{
    /**
     * @Route("/avis", name="avis")
     */
    public function index(AnnonceRepository $annonceRepository, AvisRepository $avisRepository)
    {
        return $this->render('avis/index.html.twig', [
            'annonces' => $annonceRepository->findBy(['idUserId' => null]),
            //'commentaires' => $avisRepository->findBy(['idUser' => null ]),

        ]);
    }

    /**
     * @Route("/commentaire/{id}", name="afficher_avis" )
     */
    public function afficherAvis(Annonce $annonce , AvisRepository $avisRepository, NotificationRepository $repository): Response
    {
        $em = $this->getDoctrine()->getManager();

        $notifications = $repository->findBy(['vu'=>0]);
        foreach ( $notifications as $n){
            $ch = $n->getNotification();
            $msg = substr($ch, 0, strpos($ch, ':') - 1);

            if ($msg == "Vous avez un nouveau commentaire"){
                if (substr($ch, strpos($ch, ':') + 1, strlen($msg)) ==$annonce->getId()){
                    $n->setVu(1);
                    $em->persist($n);
                    $em->flush();
                }
            }
        }
        $avis = $avisRepository ->findBy(['idAnnonce'=>$annonce]);
        return $this->render('avis/show.html.twig', [
            'commentaires' => $avis,
            'idAnnonce' => $annonce->getId()  ,

        ]);

    }

    /**
     * @Route("/commentaire/{id}/{idAnnonce}" , name="supprimer_avis")
     */

    public function supprimer(Avis $avis, $idAnnonce)
    {

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($avis);
        $entityManager->flush();

       return $this->redirectToRoute('afficher_avis', ['id' => $idAnnonce]);
    }
}

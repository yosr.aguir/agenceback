<?php

namespace App\Controller;
use App\Form\MaisonType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Annonce;
use App\Entity\Images;
use App\Repository\ImagesRepository;

class MaisonController extends AbstractController
{
    /**
     * @Route("/maison", name="maison")
     */
    public function index()
    {
        return $this->render('maison/index.html.twig', [
            'controller_name' => 'MaisonController',
        ]);
    }



    /**
     * @Route("/maison/new" , name="new_maison",requirements={"id":"\d+"})
     * Method({"GET" , "POST"})
     */

    public function new( Request $request)
    {

        $annonce = new Annonce() ;
        //dump($request->request->all());exit;

        $form = $this->createForm(MaisonType::class, $annonce);
        $form->handleRequest($request);

      if ($form->isSubmitted() && $form->isValid()) {

            $annonce = $form->getData() ;


            $entityManager = $this->getDoctrine()->getManager();
           // $annonce->setReference(self::getReference());
            $annonce->setIdAdmin($this->getUser());
            $entityManager->persist($annonce);
            $entityManager->flush();

            return $this->redirectToRoute('annonce_index' );
        }

        return $this->redirectToRoute('annonce_new',['id'=>1]);
    }
    /**
     * @Route("/maison/edit/{id}" , name="maison_edit",requirements={"id":"\d+"})
     * Method({"GET" , "POST"})
     */
    public function edit(Request $request, Annonce $annonce ,ImagesRepository $imagesRepository)
    {
        $images = $imagesRepository->findBy(['idAnnonce'=>$annonce]);
        $form = $this->createForm(MaisonType::class, $annonce);
        $form->handleRequest($request);

        if ($form->isSubmitted() ) {
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                if ($_FILES['picProfilMaison']['name'] !== '') {
                    $path = $_FILES['picProfilMaison']['name'];
                    $type = pathinfo($path, PATHINFO_EXTENSION);
                    $data = file_get_contents($_FILES['picProfilMaison']['tmp_name']);
                    $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
                    $annonce->setImgProfil($base64);


                    $em->persist($annonce);
                    $em->flush();
                }
                for ($i = 0; $i < sizeof($_FILES['picMaison']['name']); $i++) {
                    $path = $_FILES['picMaison']['name'][$i];
                    $type = pathinfo($path, PATHINFO_EXTENSION);
                    $data = file_get_contents($_FILES['picMaison']['tmp_name'][$i]);
                    $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
                    $image = new Images();
                    $image->setImg($base64)
                        ->setIdAnnonce($annonce);

                    $em->persist($image);
                    $em->flush();

                    $this->addFlash(
                        'info',
                        'Annonce Bien Modifiée'
                    );
                }

                return $this->redirectToRoute('annonce_index', array('id' => $annonce->getId()));
            }else {
                return $this->render('maison/edit.html.twig', array(
                    'annonce' => $annonce,
                    'form' => $form->createView(),
                    'images'=> $images,
                ));
            }
        }
        return $this->render('maison/edit.html.twig', array(
            'annonce' => $annonce,
            'form' => $form->createView(),
            'images'=> $images,
        ));
    }

    /**
     * @Route("/{id}", name="maison_show", methods={"GET"},requirements={"id":"\d+"})
     */
    public function show(Annonce $annonce ,ImagesRepository $imagesRepository): Response
    {
        $images = $imagesRepository->findby(['idAnnonce' =>$annonce]);
        return $this->render('maison/show.html.twig', [
            'annonce' => $annonce,
            'id' => 1,
            'images'=>$images,
        ]);
    }



}

<?php

namespace App\Controller;

use App\Form\TerrainIndustrielType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Annonce;
use App\Entity\Images;
use App\Repository\ImagesRepository;

class TerrainIndustrielController extends AbstractController
{
    /**
     * @Route("/terrain/industriel", name="terrain_industriel")
     */
    public function index()
    {
        return $this->render('terrain_industriel/index.html.twig', [
            'controller_name' => 'TerrainIndustrielController',
        ]);
    }
    /**
     * @Route("/terrainIndustriel/new" , name="new_terrain_industriel")
     * Method({"GET" , "POST"})
     */
    public function new( Request $request)
    {
        $annonce = new Annonce() ;
        $form = $this->createForm(TerrainIndustrielType::class, $annonce);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $annonce = $form->getData() ;

            $entityManager = $this->getDoctrine()->getManager();
            $annonce->setIdAdmin($this->getUser());
            $entityManager->persist($annonce);
            $entityManager->flush();


            return $this->redirectToRoute('annonce_index');
        }
        return $this ->render('annonce_new',['id'=>4]);
    }
    /**
     * @Route("/terrainIndustreil/edit/{id}" , name="edit_terrain_industreil",requirements={"id":"\d+"})
     * Method({"GET" , "POST"})
     */
    public function edit(Request $request, Annonce $annonce , ImagesRepository $imagesRepository)
    {
        $images = $imagesRepository->findBy(['idAnnonce'=>$annonce]);
        $form = $this->createForm(TerrainIndustrielType::class, $annonce);
        $form->handleRequest($request);

        if ($form->isSubmitted()){
            if($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                if ($_FILES['picProfilIndus']['name'] !== '') {
                    $path = $_FILES['picProfilIndus']['name'];
                    $type = pathinfo($path, PATHINFO_EXTENSION);
                    $data = file_get_contents($_FILES['picProfilIndus']['tmp_name']);
                    $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
                    $annonce->setImgProfil($base64);
                }
            $em->persist($annonce);
            $em->flush();
            for ($i=0; $i<sizeof($_FILES['picIndus']['name']); $i++){
                $path = $_FILES['picIndus']['name'][$i];
                $type = pathinfo($path, PATHINFO_EXTENSION);
                $data = file_get_contents($_FILES['picIndus']['tmp_name'][$i]);
                $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
                $image = new Images();
                $image->setImg($base64)
                    ->setIdAnnonce($annonce);


                $em->persist($image);
                $em->flush();

                $this->addFlash(
                    'info',
                    'Annonce Bien Modifiée'
                );
            }
            return $this->redirectToRoute('annonce_index', array('id' => $annonce->getId()));
        }else {
                    return $this->render('terrain_industriel/edit.html.twig', array(
                        'annonce' => $annonce,
                        'form' => $form->createView(),
                        'images'=>$images
                    ));
                }
            }
        return $this->render('terrain_industriel/edit.html.twig', array(
            'annonce' => $annonce,
            'form' => $form->createView(),
            'images'=>$images
        ));
    }

    /**
     * @Route("/{id}", name="show_terrain_industreil", methods={"GET"},requirements={"id":"\d+"})
     */
    public function show(Annonce $annonce, ImagesRepository $imagesRepository ): Response
    {

        $images = $imagesRepository->findBy(['idAnnonce'=>$annonce]);
        return $this->render('terrain_industreil/show.html.twig', [
            'annonce' => $annonce,
            'id' => 4,
            'images'=>$images,
        ]);
    }
}

<?php

namespace App\Controller;
use App\Form\AppartementType;
use App\Repository\ImagesRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Annonce;
use App\Entity\Images;

class AppartementController extends AbstractController
{
    /**
     * @Route("/appartement", name="appartement")
     */
    public function index()
    {

        return $this->render('appartement/index.html.twig', [
            'controller_name' => 'AppartementController',
        ]);
    }


    /**
     * @Route("/new", name="new_appartement")
     */

    public function new(Request $request)
    {

        $annonce = new Annonce() ;

        $form = $this->createForm(AppartementType::class, $annonce);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {

            if ($form->isValid()){
                $annonce = $form->getData() ;
                $entityManager = $this->getDoctrine()->getManager();
                $annonce->setIdAdmin($this->getUser());
                $entityManager->persist($annonce);
                $entityManager->flush();
                return $this->redirectToRoute('annonce_index');

            }
        }

        return $this->render('annonce/new.html.twig',
            [
                'id'=>0,
                'formAppartement' => $form->createView(),
            ]);
    }
    /**
     * @Route("/appartement/edit/{id}" , name="appartement_edit")
     * Method({"GET" , "POST"})
     */
    public function edit(Request $request, Annonce $annonce,ImagesRepository $imagesRepository)
    {

        $images = $imagesRepository->findBy(['idAnnonce' => $annonce]);
        $form = $this->createForm(AppartementType::class, $annonce);
        $form->handleRequest($request);

        if ($form->isSubmitted() ) {
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                if ($_FILES['picProfil']['name'] !== '') {
                    $path = $_FILES['picProfil']['name'];
                    $type = pathinfo($path, PATHINFO_EXTENSION);
                    $data = file_get_contents($_FILES['picProfil']['tmp_name']);
                    $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
                    $annonce->setImgProfil($base64);
                }

                $em->persist($annonce);
                $em->flush();

                for ($i = 0; $i < sizeof($_FILES['pic']['name']); $i++) {
                    if ($_FILES['pic']['name'] !== '') {
                        $path = $_FILES['pic']['name'][$i];
                        $type = pathinfo($path, PATHINFO_EXTENSION);
                        $data = file_get_contents($_FILES['pic']['tmp_name'][$i]);
                        $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);

                        $image = new Images();
                        $image->setImg($base64)
                            ->setIdAnnonce($annonce);
                    }
                    $em->persist($image);
                    $em->flush();
                    $this->addFlash(
                        'info',
                        'Annonce Bien Modifiée'
                    );
                }
                
                return $this->redirectToRoute('annonce_index', array('id' => $annonce->getId()));

            } else {
                return $this->render('appartement/edit.html.twig', array(
                    'annonce' => $annonce,
                    'form' => $form->createView(),
                    'images' => $images,
                ));
            }
        }
        return $this->render('appartement/edit.html.twig', array(
            'annonce' => $annonce,
            'form' => $form->createView(),
            'images'=>$images,

        ));
    }

    /**
     * @Route("/{id}/show", name="appartement_show", methods={"GET"},requirements={"id":"\d+"})
     */
    public function show(Annonce $annonce, ImagesRepository $imagesRepository): Response
    {
        $images = $imagesRepository->findBy(['idAnnonce'=>$annonce]);
        return $this->render('appartement/show.html.twig', [
            'annonce' => $annonce,
            'id' => 0,
            'images'=> $images,

        ]);
    }

    /**
     * @Route("/delete/{id}/{idImage}", name="appartement_delete", methods={"GET"})
     */
    public function delete(Request $request, Annonce $annonce , $idImage ): Response
    {

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($annonce);
            $entityManager->flush();
        {
                return $this->redirectToRoute('annonce_index', ['id' => $idImage]);

        }

        //return $this->redirectToRoute('annonce_index');

    }
}

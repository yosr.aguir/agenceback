<?php

namespace App\Controller;

use App\Form\TerrainConstructibleType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Annonce;
use App\Entity\Images;
use App\Repository\ImagesRepository;

class TerrainConstructibleController extends AbstractController
{
    /**
     * @Route("/terrain/constructible", name="terrain_constructible")
     */
    public function index()
    {
        return $this->render('terrain_constructible/index.html.twig', [
            'controller_name' => 'TerrainConstructibleController',
        ]);
    }
    /**
     * @Route("/terrainConstructible/new" , name="new_terrain_constructible")
     * Method({"GET" , "POST"})
     */

    public function new( Request $request)
    {
        $annonce = new Annonce() ;
        $form = $this->createForm(TerrainConstructibleType::class, $annonce);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $annonce = $form->getData() ;

            $entityManager = $this->getDoctrine()->getManager();
            $annonce->setIdAdmin($this->getUser());
            $entityManager->persist($annonce);
            $entityManager->flush();


            return $this->redirectToRoute('annonce_index');
        }

        return $this ->render('annonce_new',['id'=>5]);
    }


    /**
     * @Route("/terrainConstrutible/edit/{id}" , name="edit_terrain_constructible",requirements={"id":"\d+"})
     * Method({"GET" , "POST"})
     */
    public function edit(Request $request, Annonce $annonce , ImagesRepository $imagesRepository )
    {
        $images = $imagesRepository->findBy(['idAnnonce'=>$annonce]);
        $form = $this->createForm(TerrainConstructibleType::class, $annonce);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $path = $_FILES['picProfilCons']['name'];
            $type = pathinfo($path, PATHINFO_EXTENSION);
            $data = file_get_contents($_FILES['picProfilCons']['tmp_name']);
            $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
            $annonce->setImgProfil($base64);

            $em = $this->getDoctrine()->getManager();
            $em->persist($annonce);
            $em->flush();

            for ($i=0; $i<sizeof($_FILES['picConstructible']['name']); $i++){
                $path = $_FILES['picConstructible']['name'][$i];
                $type = pathinfo($path, PATHINFO_EXTENSION);
                $data = file_get_contents($_FILES['picConstructible']['tmp_name'][$i]);
                $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
                $image = new Images();
                $image->setImg($base64)
                    ->setIdAnnonce($annonce);


                $em->persist($image);
                $em->flush();
                $this->addFlash(
                    'info',
                    'Annonce Bien Modifiée'
                );
            }

            return $this->redirectToRoute('annonce_index', array('id' => $annonce->getId()));
        }

        return $this->render('terrain_constructible/edit.html.twig', array(
            'annonce' => $annonce,
            'form' => $form->createView(),
            'images'=>$images
        ));
    }

    /**
     * @Route("/{id}", name="show_terrain_constructible", methods={"GET"},requirements={"id":"\d+"})
     */
    public function show(Annonce $annonce , ImagesRepository $imagesRepository ): Response
    {
dump('wlh lina');exit;
        $images = $imagesRepository->findBy(['idAnnonce'=>$annonce]);
        return $this->render('terrain_constructible/show.html.twig', [
            'annonce' => $annonce,
            'id' => 5,
            'images'=>$images,

        ]);
    }
}

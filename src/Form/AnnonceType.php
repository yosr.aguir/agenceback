<?php

namespace App\Form;

use App\Entity\Annonce;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Constraints;

class AnnonceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('reference', TextType::class,[

                'required' => true,
                'label' => 'réference',
                'attr' => [
                    'autocomplete' => 'off',
                    'placeholder' => 'réference...',
                    'class' => 'form-control',
                ],
                'constraints' => [
                    new Constraints\NotBlank(),
                    new Constraints\Regex([
                        'pattern' => "/^[a-zA-Z]+$/",
                        'match' => true,
                        'message' => "Format invalide"]),

                ],

            ])


            ->add('typeAction' ,ChoiceType::class)

        ->add('nature',TextType::class,[

            'required' => true,
            'label' => 'Nature',
            'attr' => [
                'autocomplete' => 'off',
                'placeholder' => 'Nature...',
                'class' => 'form-control',
            ],
            'constraints' => [
                new Constraints\NotBlank(),
                new Constraints\Regex([
                    'pattern' => "/^[a-zA-Z]+$/",
                    'match' => true,
                    'message' => "Format invalide"]),

            ],

        ])
            ->add('prix' , MoneyType::class)
            ->add('ville', TextType::class,[

                'required' => true,
                'label' => 'ville',
                'attr' => [
                    'autocomplete' => 'off',
                    'placeholder' => 'Ville...',
                    'class' => 'form-control',
                ],
                'constraints' => [
                    new Constraints\NotBlank(),
                    new Constraints\Regex([
                        'pattern' => "/^[a-zA-Z]+$/",
                        'match' => true,
                        'message' => "Format invalide"]),

                ],

            ])
            ->add('adresse', TextType::class ,[

                'required' => true,
                'label' => 'Adresse',
                'attr' => [
                    'autocomplete' => 'off',
                    'placeholder' => 'Adresse...',
                    'class' => 'form-control',
                ],
                'constraints' => [
                    new Constraints\NotBlank(),
                    new Constraints\Regex([
                        'pattern' => "/^[a-zA-Z]+$/",
                        'match' => true,
                        'message' => "Format invalide"]),

                ],

            ])
            ->add('surface' ,IntegerType::class )
            ->add('titreFinancier' ,TextType::class )
            ->add('mandatExlusive' , IntegerType::class)
            ->add('fraisAgence', IntegerType::class )
            ->add('nbrPieces',IntegerType::class )
            ->add('nbrEtage', IntegerType::class)

            ->add('options', ChoiceType::class,[
                'choices'=>['a'=>'a','b'=>'b','c'=>'c'],
                'multiple'=> true
            ])
            ->add('ContactEmail')
            ->add('ContactTel')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Annonce::class,
        ]);
    }
}

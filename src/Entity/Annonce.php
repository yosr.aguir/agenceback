<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Annonce
 *
 * @ORM\Table(name="annonce")
 * @ORM\Entity
 */
class Annonce
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="reference", type="string", length=255, nullable=false)
     */
    private $reference;

    /**
     * @var string
     *
     * @ORM\Column(name="type_action", type="string", length=255, nullable=false)
     */
    private $typeAction;

    /**
     * @var string
     *
     * @ORM\Column(name="nature", type="string", length=255, nullable=false)
     */
    private $nature;

    /**
     * @var string
     *
     * @ORM\Column(name="prix", type="string", length=255, nullable=false)
     */
    private $prix;

    /**
     * @var string
     *
     * @ORM\Column(name="ville", type="string", length=255, nullable=false)
     */
    private $ville;

    /**
     * @var string
     *
     * @ORM\Column(name="adresse", type="string", length=255, nullable=false)
     */
    private $adresse;

    /**
     * @var string
     *
     * @ORM\Column(name="surface", type="string", length=255, nullable=false)
     */
    private $surface;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="titre_financier", type="boolean", nullable=true)
     */
    private $titreFinancier;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="mandat_exlusive", type="boolean", nullable=true)
     */
    private $mandatExlusive;

    /**
     * @var string
     *
     * @ORM\Column(name="frais_agence", type="string", length=255, nullable=false)
     */
    private $fraisAgence;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nbr_pieces", type="string", length=255, nullable=true)
     */
    private $nbrPieces;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nbr_etage", type="string", length=255, nullable=true)
     */
    private $nbrEtage;

    /**
     * @var array|null
     *
     * @ORM\Column(name="options", type="array", length=0, nullable=true)
     */
    private $options;

    /**
     * @var string
     *
     * @ORM\Column(name="contact_email", type="string", length=255, nullable=false)
     */
    private $contactEmail;

    /**
     * @var int
     *
     * @ORM\Column(name="contact_tel", type="integer", nullable=false)
     */
    private $contactTel;

    /**
     * @var int|null
     *
     * @ORM\Column(name="id_user_id", type="integer", nullable=true)
     */
    private $idUserId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="id_admin_id", type="integer", nullable=true)
     */
    private $idAdminId;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="date_add", type="date", nullable=true)
     */
    private $dateAdd;

    /**
     * @var string|null
     *
     * @ORM\Column(name="img_profil", type="text", length=0, nullable=true)
     */
    private $imgProfil;

    private $avis;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $active;

    public function __construct()
    {
        $this->messages = new ArrayCollection();
        $this->avis = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getReference(): ?string
    {
        return $this->reference;
    }

    public function setReference(string $reference): self
    {
        $this->reference = $reference;

        return $this;
    }

    public function getTypeAction(): ?string
    {
        return $this->typeAction;
    }

    public function setTypeAction(string $typeAction): self
    {
        $this->typeAction = $typeAction;

        return $this;
    }

    public function getNature(): ?string
    {
        return $this->nature;
    }

    public function setNature(string $nature): self
    {
        $this->nature = $nature;

        return $this;
    }

    public function getPrix(): ?string
    {
        return $this->prix;
    }

    public function setPrix(string $prix): self
    {
        $this->prix = $prix;

        return $this;
    }

    public function getVille(): ?string
    {
        return $this->ville;
    }

    public function setVille(string $ville): self
    {
        $this->ville = $ville;

        return $this;
    }

    public function getAdresse(): ?string
    {
        return $this->adresse;
    }

    public function setAdresse(string $adresse): self
    {
        $this->adresse = $adresse;

        return $this;
    }

    public function getSurface(): ?string
    {
        return $this->surface;
    }

    public function setSurface(string $surface): self
    {
        $this->surface = $surface;

        return $this;
    }

    public function getTitreFinancier(): ?bool
    {
        return $this->titreFinancier;
    }

    public function setTitreFinancier(?bool $titreFinancier): self
    {
        $this->titreFinancier = $titreFinancier;

        return $this;
    }

    public function getMandatExlusive(): ?bool
    {
        return $this->mandatExlusive;
    }

    public function setMandatExlusive(?bool $mandatExlusive): self
    {
        $this->mandatExlusive = $mandatExlusive;

        return $this;
    }

    public function getFraisAgence(): ?string
    {
        return $this->fraisAgence;
    }

    public function setFraisAgence(string $fraisAgence): self
    {
        $this->fraisAgence = $fraisAgence;

        return $this;
    }

    public function getNbrPieces(): ?string
    {
        return $this->nbrPieces;
    }

    public function setNbrPieces(?string $nbrPieces): self
    {
        $this->nbrPieces = $nbrPieces;

        return $this;
    }

    public function getNbrEtage(): ?string
    {
        return $this->nbrEtage;
    }

    public function setNbrEtage(?string $nbrEtage): self
    {
        $this->nbrEtage = $nbrEtage;

        return $this;
    }

    public function getOptions(): ?array
    {
        return $this->options;
    }

    public function setOptions(?array $options): self
    {
        $this->options = $options;

        return $this;
    }

    public function getContactEmail(): ?string
    {
        return $this->contactEmail;
    }

    public function setContactEmail(string $contactEmail): self
    {
        $this->contactEmail = $contactEmail;

        return $this;
    }

    public function getContactTel(): ?int
    {
        return $this->contactTel;
    }

    public function setContactTel(int $contactTel): self
    {
        $this->contactTel = $contactTel;

        return $this;
    }

    public function getIdUserId(): ?int
    {
        return $this->idUserId;
    }

    public function setIdUserId(?int $idUserId): self
    {
        $this->idUserId = $idUserId;

        return $this;
    }

    public function getIdAdminId(): ?int
    {
        return $this->idAdminId;
    }

    public function setIdAdminId(?int $idAdminId): self
    {
        $this->idAdminId = $idAdminId;

        return $this;
    }

    public function getDateAdd(): ?\DateTimeInterface
    {
        return $this->dateAdd;
    }

    public function setDateAdd(?\DateTimeInterface $dateAdd): self
    {
        $this->dateAdd = $dateAdd;

        return $this;
    }

    public function getImgProfil(): ?string
    {
        return $this->imgProfil;
    }

    public function setImgProfil(?string $imgProfil): self
    {
        $this->imgProfil = $imgProfil;

        return $this;
    }

    public function addAvi(Avis $avi): self
    {
        if (!$this->avis->contains($avi)) {
            $this->avis[] = $avi;
            $avi->setIdAnnonce($this);
        }

        return $this;
    }

    public function removeAvi(Avis $avi): self
    {
        if ($this->avis->contains($avi)) {
            $this->avis->removeElement($avi);
            // set the owning side to null (unless already changed)
            if ($avi->getIdAnnonce() === $this) {
                $avi->setIdAnnonce(null);
            }
        }

        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(?bool $active): self
    {
        $this->active = $active;

        return $this;
    }


}
